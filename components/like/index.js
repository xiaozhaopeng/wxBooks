Component({
  /**
   * 组件的属性列表
   */
  properties: {
    like: {
      type: Boolean,
      //  value:false,
      //  observer:function(){}
    },
    count: {
      type: Number,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    yesSrc: 'images/a1.png',
    noSrc: 'images/a2.png'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onLike(event) {
      let like = this.properties.like
      let count = this.properties.count
      count = like ? count - 1 : count+1
      this.setData({
        count: count,
        like: !like,
      })
    },
  }
})

